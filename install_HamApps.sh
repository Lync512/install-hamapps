#Copyright 2019 Jonathan Moseley

#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, 
#including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
#and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
#IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#


echo "Install ham radio v0.1"
echo "Basic Ham radio program install script for Ubuntu 18.04.2 LTS"
echo "Installs: GQRX, GNURadio, Fldigi, CQRLog"
echo "software pulled is copyright per it's respective owners"
echo "This script assumes the user" $USER "has sudo permissions" 
echo ""
echo "Now installing the software"
echo ""
echo "Cleaning system..."
sudo apt-get purge --auto-remove gqrx
sudo apt-get purge --auto-remove gqrx-sdr
sudo apt-get purge --auto-remove libgnuradio*
echo ""
echo "Now to the final install, this could take a while."
sudo add-apt-repository -y ppa:bladerf/bladerf
sudo add-apt-repository -y ppa:myriadrf/drivers
sudo add-apt-repository -y ppa:myriadrf/gnuradio
sudo add-apt-repository -y ppa:gqrx/gqrx-sdr
sudo add-apt-repository -y ppa:kamalmostafa/fldigi
sudo apt-add-repository -y ppa:dansmith/chirp-snapshots
sudo add-apt-repository -y ppa:ok2cqr/ppa
sudo apt-get update

sudo apt install -y gqrx-sdr --install-suggests
sudo apt install -y gnuradio --install-suggests
sudo apt install -y fldigi
sudo apt install -y chirp-daily
sudo apt install -y cqrlog

echo "Adding" $USER "to dialout, please log out and log back in for this to take effect"
sudo adduser $USER dialout

echo "Thanks for using this script!"
echo "73"
echo "script by John/KE8GAH"

